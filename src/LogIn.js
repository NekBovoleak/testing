import React, {Component} from 'react'
import {Text, View, ImageBackground,Image} from 'react-native'
import {Button, Icon} from 'native-base'

class LogIn extends Component {
    render(){
        return(
            <ImageBackground 
                source={require('./image/background.jpg')} 
                style={{width: '100%', height:'100%',flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Image
                    source={require('./image/logo-White.png')}
                    style={{width:193, height:70}}
                />
                <Text style={{color: 'white', fontFamily: 'Roboto', fontSize: 10}}>The Food Recipe Mobile App in Cambodia</Text>
                
                <Button iconLeft style={{backgroundColor: '#F58020', alignSelf: 'center', marginTop: 30, width: 220}}>
                    <Icon name='logo-whatsapp' />
                    <Text style={{fontFamily: 'Koulen', color: 'white', marginLeft: 10, marginRight: 30}}>ភ្ជាប់គណនីតាមលេខទូរស័ព្ទ</Text>
                </Button>

                <Button iconLeft style={{backgroundColor: '#475993', alignSelf: 'center', marginTop: 15, width: 220}}>
                    <Icon name='logo-facebook' style={{color: 'white'}} />
                    <Text style={{fontFamily: 'Koulen', color: 'white', marginLeft: 10, marginRight: 30}}>ភ្ជាប់ជាមួយគណនី Facebook</Text>
                </Button>

            </ImageBackground>
        );
    }
}

export default LogIn;